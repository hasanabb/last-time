import { createApp } from 'vue';
import App from './App.vue';
import router from './router.js';
import store from './store';

import BaseSpinner from './components/BaseSpinner.vue';
import BaseModal from './components/BaseModal.vue';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import VCalendar from 'v-calendar';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCalendarAlt, faPencilAlt, faCheck, faTrashAlt, faStopwatch, faLayerGroup } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

const app = createApp(App);

library.add(faCalendarAlt, faPencilAlt, faCheck, faTrashAlt, faStopwatch, faLayerGroup);

app.use(router);
app.use(store);
app.use(VCalendar, {
    mode: 'dateTime'
});
app.component('base-spinner', BaseSpinner);
app.component('base-modal', BaseModal);
app.component('fa-icon', FontAwesomeIcon);

app.mount('#app');