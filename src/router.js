import { createRouter, createWebHistory } from 'vue-router';
import Home from './views/Home.vue';
import store from './store/index.js';
const NotFound = () =>
    import ('./views/NotFound.vue');
const About = () =>
    import ('./views/About.vue');
const UserAuth = () =>
    import ('./views/auth/UserAuth.vue');

const routes = [
    { path: '/', redirect: '/home' },
    { path: '/home', name: 'Home', component: Home, meta: { auth: true } },
    { path: '/about', name: 'About', component: About, meta: { auth: true } },
    { path: '/auth', name: 'Auth', component: UserAuth, meta: { unauth: true } },
    { path: '/:notFound(.*)', component: NotFound }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

router.beforeEach((to, _, next) => {
    if (to.meta.auth && !store.getters.isAuthenticated) {
        next('/auth');
    } else if (to.meta.unauth && store.getters.isAuthenticated) {
        next();
    } else {
        next();
    }
});

export default router