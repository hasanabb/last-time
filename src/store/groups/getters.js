export default {
    groups(state) {
        return state.groups;
    },
    hasGroups(_, getters) {
        return getters.groups && getters.groups.length > 0;
    }
};