const BASE_URL = 'https://last-time-cfd0a-default-rtdb.europe-west1.firebasedatabase.app';

export default {
    async addGroup(context, payload) {
        const userId = context.rootGetters.userId;
        const request = {
            name: payload.group,
            userId
        };
        const token = context.rootGetters.token;
        const url = `${BASE_URL}/groups/${userId}.json?auth=${token}`;
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(request)
        });
        const data = await response.json();

        if (!response.ok) {
            const error = new Error(response.message || 'Failed to fetch!');
            throw error;
        }

        request.id = data.name;
        request.coachId = payload.coachId;
        context.commit('addGroup', request);
    },
    async loadGroups(context) {
        const token = context.rootGetters.token;
        const userId = context.rootGetters.userId;
        const url = `${BASE_URL}/groups/${userId}.json?auth=${token}`;
        const response = await fetch(url);
        const data = await response.json();

        if (!response.ok) {
            const error = new Error(response.message || 'Failed to fetch!');
            throw error;
        }

        const groups = [];

        for (const key in data) {
            const group = {
                id: key,
                name: data[key].name
            };
            groups.push(group);
        }

        context.commit('setGroups', groups);
    },
    async deleteGroup(context, payload) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const url = `${BASE_URL}/groups/${userId}/${payload.id}.json?auth=${token}`;
        const response = await fetch(url, {
            method: 'DELETE'
        });

        if (!response.ok) {
            const error = new Error(response.message || 'Failed to fetch!');
            throw error;
        }

        context.getters
    }
};