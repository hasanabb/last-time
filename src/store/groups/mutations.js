export default {
    addGroup(state, payload) {
        state.groups.push(payload);
    },
    setGroups(state, payload) {
        state.groups = payload;
    },
    deleteGroup(state, payload) {
        const id = payload.id;
        state.groups = state.groups.filter(g => g.id !== id);
    }
};