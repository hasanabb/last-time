export default {
    entries(state, _, _2, rootGetters) {
        const userId = rootGetters.userId;
        return state.entries.filter(e => e.userId === userId);
    },
    hasEntries(_, getters) {
        return getters.entries && getters.entries.length > 0;
    }
};