export default {
    addEntry(state, payload) {
        state.entries.push(payload);
    },
    setEntries(state, payload) {
        state.entries = payload;
    }
};