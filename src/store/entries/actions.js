import moment from "moment";

const BASE_URL = 'https://last-time-cfd0a-default-rtdb.europe-west1.firebasedatabase.app';

export default {
    async addEntry(context, payload) {
        const userId = context.rootGetters.userId;
        const request = {
            entry: payload.entry,
            time: payload.time,
            group: payload.group,
            notes: payload.notes,
            reminder: payload.remindMe,
            userId
        };
        const token = context.rootGetters.token;
        const url = `${BASE_URL}/entries/${userId}.json?auth=${token}`;
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(request)
        });
        const data = await response.json();

        if (!response.ok) {
            const error = new Error(response.message || 'Failed to fetch!');
            throw error;
        }

        request.id = data.name;
        request.coachId = payload.coachId;
        context.commit('addEntry', request);
    },
    async loadEntries(context) {
        const token = context.rootGetters.token;
        const userId = context.rootGetters.userId;
        const url = `${BASE_URL}/entries/${userId}.json?auth=${token}`;
        const response = await fetch(url);
        const data = await response.json();

        if (!response.ok) {
            const error = new Error(response.message || 'Failed to fetch!');
            throw error;
        }

        const entries = [];

        for (const key in data) {
            const now = moment();
            const reminder = moment(data[key].remindMe);
            var duration = moment.duration(reminder.diff(now));

            const entry = {
                id: key,
                userId,
                entry: data[key].entry,
                group: data[key].group,
                time: data[key].time,
                notes: data[key].notes,
                reminder: data[key].remindMe,
                cron: duration.as('milliseconds')
            };
            entries.push(entry);
        }

        context.commit('setEntries', entries);
    }
};