import { createStore } from 'vuex';

import auth from './auth/index.js';
import entries from './entries/index.js';
import groups from './groups/index.js';

export default createStore({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth,
        entries,
        groups
    }
})